# avatar

A bash script to create an avatar based on your desktop.

More info @ [readme.txt](https://gitlab.com/apgomes/avatar/blob/master/readme.txt).
