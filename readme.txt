avatar -- A bash script to create an avatar based on your desktop
v0.5.3 (20181202)
https://gitlab.com/apgomes/avatar

Usage: ./avatar x, where 'x' is the image size in pixels
Type -h for help

ImageMagick is required to be installed.
(macOS users: install imagemagick from MacPorts)

Please note: The maximum image size is set to 128x128 pixels. You can
edit/modify the script and set it to a new value if you prefer. But don't forget
to also update blank.png accordingly.

Tested on Xubuntu 18.04.1 // macOS Mojave 10.14.1
